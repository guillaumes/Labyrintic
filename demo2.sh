#!/bin/bash

bundle install --path vendor/bundle

bundle exec ruby src/generate.rb 950 1000 20 depth,prim,kruskal maze/demo.sg & \

bundle exec ruby src/solve.rb 20 depth,flood,astar maze/demo.sg