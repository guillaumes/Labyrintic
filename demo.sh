#!/bin/bash

bundle install --path vendor/bundle

bundle exec ruby src/generate.rb 500 500 20 kruskal maze/kruskal.sg & \
bundle exec ruby src/generate.rb 500 500 20 depth maze/depth.sg & \
bundle exec ruby src/generate.rb 500 500 20 prim maze/prim.sg &\

bundle exec ruby src/solve.rb 20 astar maze/depth.sg & \
bundle exec ruby src/solve.rb 20 flood maze/depth.sg & \
bundle exec ruby src/solve.rb 20 depth maze/depth.sg