# Maze

Ruby/Gosu maze generation and solving

## Try it

Install ruby and bundler

```
bundle install --path vendor/bundle
bundle exec ruby src/generate.rb 1920 1080 100 kruskal,depth,prim maze/1.maze
bundle exec ruby src/solve.rb 40 flood,depth,a maze/1.maze 0
```

```
bundle exec ruby src/generate.rb 1000 1000 30 & bundle exec ruby src/solve.rb 30
```

# Demo
```
chmod +x demo.sh
./demo.sh
```
Arrange windows as shown in demo-example.png 

![demo-example.png](//gitlab.etude.cergy.eisti.fr/guillaumes/Labyrintic/raw/master/demo-example.png)
