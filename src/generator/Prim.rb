require 'gosu'

require_relative 'AbstractGenerator.rb'
require_relative '../maze/Maze.rb'
require_relative '../maze/Point.rb'

class Prim < AbstractGenerator
 
  # width and height in number of cell
  def initialize w, h, cs, wc
    @width = w
    @height = h
    @cellSize = cs

    @maze = Maze.new @width, @height, @cellSize, wc

    cx = Random.rand w
    cy = Random.rand h
    @cursors = Array.new 
    @cursors << Point.new(cx, cy)

    # Just for a nicer draw
    @visited = Array.new(@width)
    @width.times do |x|
      @visited[x] = Array.new(@height)
      @height.times do |y|
        @visited[x][y] = false
      end
    end

    @generated = false
  end

  def step
    if !@generated
      if @cursors.length == 0
        @generated = true
      else
        (@cursors.length.to_f/10).ceil.times do
          cursor = @cursors.sample
          @visited[cursor.x][cursor.y] = true
          ccell = @maze.cells[cursor.x][cursor.y]

          cells = [@maze.cellUp(ccell), @maze.cellDown(ccell), @maze.cellLeft(ccell), @maze.cellRight(ccell)]
          walls = [@maze.wallUp(ccell), @maze.wallDown(ccell), @maze.wallLeft(ccell), @maze.wallRight(ccell)]
          dir = %w{up down left right}

          # Keep cell that exist with a wall in the way
          choices = cells.zip(walls, dir).select { |z| !z[0].nil? && z[1]}
          choices.select! do |z|
            cell = z[0]
            walls = [@maze.wallUp(cell), @maze.wallDown(cell), @maze.wallLeft(cell), @maze.wallRight(cell)]
            walls.select { |w| !w }.length < 1
          end

          @cursors.delete cursor if choices.length <= 1
          return if choices.length == 0

          choice = choices.sample
          goto = case choice[2]
            when "up"
              @maze.wallUp(ccell, false)
              Point.new(cursor.x, cursor.y-1)
            when "down"
              @maze.wallDown(ccell, false)            
              Point.new(cursor.x, cursor.y+1)
            when "left"
              @maze.wallLeft(ccell, false)
              Point.new(cursor.x-1, cursor.y)
            when "right"
              @maze.wallRight(ccell, false)
              Point.new(cursor.x+1, cursor.y)
          end
          @cursors << goto
        end
      end
    end
  end

  def draw
    if !@generated
      @width.times do |x|
        @height.times do |y|
          draw_cell(x, y, Gosu::Color.rgba(144, 238, 144, 200)) if @visited[x][y]       
        end
      end

      @cursors.each do |cursor|
        draw_cell(cursor.x, cursor.y, Gosu::Color.rgba(255,0,0,200))
      end
    end
    @maze.draw    
  end
end