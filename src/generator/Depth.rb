require 'gosu'

require_relative 'AbstractGenerator.rb'
require_relative '../maze/Maze.rb'
require_relative '../maze/Point.rb'

class Depth < AbstractGenerator
 
  # width and height in number of cell
  def initialize w, h, cs, wc
    @width = w
    @height = h
    @cellSize = cs

    @maze = Maze.new @width, @height, @cellSize, wc

    cx = Random.rand w
    cy = Random.rand h
    @cursor = Point.new(cx, cy)

    @generated = false
    @visited_n = 1
    @visited = Array.new(@width)
    @width.times do |x|
      @visited[x] = Array.new(@height)
      @height.times do |y|
        @visited[x][y] = false
      end
    end
    @previous = Hash.new
  end

  def step
    if !@generated
      if @visited_n >= @width * @height
        @generated = true
      else
        @visited[@cursor.x][@cursor.y] = true
        cell = @maze.cells[@cursor.x][@cursor.y]
      
        dir = [@maze.cellUp(cell), @maze.cellDown(cell), @maze.cellLeft(cell), @maze.cellRight(cell)]
        vis = dir.map { |c| c.nil? || @visited[c.x][c.y] }
      
        dir = dir.zip(vis).select{ |z| !z[1] }.map{ |z| z[0] }
        goto = dir.sample
        if goto.nil?
          prev = @previous[cell]
          @cursor.x = prev.x
          @cursor.y = prev.y
        else
          @visited_n += 1
          if goto.y < cell.y
            @maze.wallUp(cell, false)
          elsif goto.y > cell.y
            @maze.wallDown(cell, false)
          elsif goto.x < cell.x
            @maze.wallLeft(cell, false)
          elsif goto.x > cell.x
            @maze.wallRight(cell, false)
          end
      
      
          @previous[goto] = cell
          @cursor.x = goto.x
          @cursor.y = goto.y
        end
      end
    end
  end

  def draw
    if !@generated

      @width.times do |x|
        @height.times do |y|
          draw_cell(x, y, Gosu::Color.rgba(144, 238, 144, 100)) if @visited[x][y]       
        end
      end

      draw_cell(@cursor.x, @cursor.y, Gosu::Color::RED)  
      cell = @maze.cells[@cursor.x][@cursor.y]
      prev = @previous[cell]
      while !prev.nil?
        draw_cell(prev.x, prev.y, Gosu::Color.rgba(144, 238, 144, 255))
        prev = @previous[prev]
      end
    end
    @maze.draw    
  end
end