require 'gosu'

require_relative 'AbstractGenerator.rb'
require_relative '../maze/Maze.rb'
require_relative '../maze/Point.rb'

class Kruskal < AbstractGenerator
  
  # width and height in number of cell
  def initialize w, h, cs, wc
    @width = w
    @height = h
    @cellSize = cs

    @maze = Maze.new @width, @height, @cellSize, wc

    i = 0
    @set = Array.new(@width)
    @walls = []
    @width.times do |x|
      @set[x] = Array.new(@height)
      @height.times do |y|
        @set[x][y] = i
        i += 1

        cell = @maze.cells[x][y]
        if !@maze.cellUp(cell).nil?
          @walls << ['up', cell, @maze.cellUp(cell)]
        end
        if !@maze.cellLeft(cell).nil?
          @walls << ['left', cell, @maze.cellLeft(cell)]
        end

      end
    end

    @colors = []
    h = 0.0
    while @colors.length < i
      @colors << Gosu::Color.from_hsv(h, 90 + Random.rand*10, 50 + Random.rand*10)      
      h += 360.0 / (i-1)
    end
    @colors = @colors.shuffle

    @generated = false
  end

  def count a
    n = 0
    @width.times do |x|
      @height.times do |y|
        i = @set[x][y]
        if i == a
          n += 1
        end        
      end
    end
    n
  end

  def move a, b
    return move b,a if count(b) > count(a)
    n = 0
    @width.times do |x|
      @height.times do |y|
        i = @set[x][y]
        if i == b
          @set[x][y] = a
          n += 1
        elsif i == a
          n += 1
        end        
      end
    end

    @walls.select! do |wall|
      cell = wall[1]
      goto = wall[2]
      @set[cell.x][cell.y] != @set[goto.x][goto.y]
    end

    if n >= @colors.length
      @generated = true
    end
  end

  def step
    if !@generated
      wall = @walls.delete_at Random.rand(@walls.length)
      cell = wall[1]
      goto = wall[2]
      
      if @set[cell.x][cell.y] != @set[goto.x][goto.y]
        move @set[cell.x][cell.y], @set[goto.x][goto.y]
        case wall[0]
          when 'up'
            @maze.wallUp(cell, false)
          when 'left'
            @maze.wallLeft(cell,false)
        end
      end
    end
  end

  def draw
    if !@generated
      @width.times do |x|
        @height.times do |y|
          i = @set[x][y]
          draw_cell(x, y, @colors[i])          
        end
      end
    end
    @maze.draw    
  end
end