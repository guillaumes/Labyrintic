require 'gosu'
require 'pp'

Point = Struct.new("Point", :x, :y)

def center a, b
  Point.new((a.x + b.x)/2.0, (a.y + b.y)/2.0)
end

class MainWindow < Gosu::Window
  def initialize width, height
    @width = width
    @height = height
    super @width, @height, false, 1.0
    self.caption = "=="

    @start = Array.new
    @start << Point.new(width/2,0)
    @start << Point.new(0,height)
    @start << Point.new(width,height)

    @points = Array.new
    @points << @start.sample

  end


  def update
    exit if Gosu.button_down? Gosu::KB_ESCAPE

    100.times {
      @points << center(@points.last, @start.sample)
    }
  end

  def draw
    Gosu.draw_rect(0, 0, @width, @height, Gosu::Color::WHITE)

    @points.each do |point|
      Gosu.draw_rect(point.x, point.y, 1,1, Gosu::Color::BLACK)
    end

    print ([8.chr]*30).join
    print "#{Gosu.fps} fps with #{@points.size} points"
  end
end

MainWindow.new(800, 600).show
