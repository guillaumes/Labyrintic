require 'gosu'

require_relative 'AbstractSolver.rb'

class Depth < AbstractSolver

  def initialize maze, from, to
    @maze = maze
    @from = @maze.cells[from.x][from.y]
    @to = @maze.cells[to.x][to.y]

    @width = @maze.width
    @height = @maze.height
    @cellSize = @maze.cellSize

    @previous = Hash.new
    @visited = Array.new(@width)
    @width.times do |x|
      @visited[x] = Array.new(@height)
      @height.times do |y|
        @visited[x][y] = false
      end
    end

    @goto = @from

    @solved = false
  end

  def step
    if !@solved
      current = @goto
      @visited[current.x][current.y] = true
      
      if current == @to
        @solved = true
      else
        dir = [
          @maze.wallUp(current) ? nil : @maze.cellUp(current), 
          @maze.wallDown(current) ? nil : @maze.cellDown(current), 
          @maze.wallLeft(current) ? nil : @maze.cellLeft(current), 
          @maze.wallRight(current) ? nil : @maze.cellRight(current)
        ]
        dir.select! { |cell| !cell.nil? and !@visited[cell.x][cell.y] }
        if dir.size > 0
          @goto = dir.min_by { |cell| (cell.x-@to.x)**2 + (cell.y-@to.y)**2}
          @previous[@goto] = current
        else
          @goto = @previous[current]
        end
      end
    end
  end

  def draw
    color = Gosu::Color.rgba(144, 238, 144, 255)

    @width.times do |x|
      @height.times do |y|
        draw_cell(x, y, Gosu::Color.rgba(0,0,0,25)) if @visited[x][y]
      end
    end

    if @solved
      current = @to
      loop do
        prev = @previous[current]
        break if prev.nil?
        draw_cell(prev.x, prev.y, color)        
        current = prev
      end
    else
      current = @goto
      draw_cell(current.x, current.y, color)
      loop do
        prev = @previous[current]
        break if prev.nil?
        draw_cell(prev.x, prev.y, color)        
        current = prev
      end
    end
    draw_cell(@from.x, @from.y, Gosu::Color::GREEN)
    draw_cell(@to.x, @to.y, Gosu::Color::RED)

    @maze.draw
  end

end