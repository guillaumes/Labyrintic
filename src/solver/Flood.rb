require 'gosu'

require_relative 'AbstractSolver.rb'

class Flood < AbstractSolver

  def initialize maze, from, to
    @maze = maze
    @from = @maze.cells[from.x][from.y]
    @to = @maze.cells[to.x][to.y]

    @width = @maze.width
    @height = @maze.height
    @cellSize = @maze.cellSize

    @previous = Hash.new
    @visited = Array.new(@width)
    @width.times do |x|
      @visited[x] = Array.new(@height)
      @height.times do |y|
        @visited[x][y] = false
      end
    end

    @currents = Array.new
    @currents << @from

    @solved = false
  end

  def step
    if !@solved
      current = @currents.delete_at Random.rand(@currents.size)
      if !@visited[current.x][current.y]
        @visited[current.x][current.y] = true
        
        if current == @to
          @solved = true
        else
          dir = [
            @maze.wallUp(current) ? nil : @maze.cellUp(current), 
            @maze.wallDown(current) ? nil : @maze.cellDown(current), 
            @maze.wallLeft(current) ? nil : @maze.cellLeft(current), 
            @maze.wallRight(current) ? nil : @maze.cellRight(current)
          ]
          dir.select! { |cell| !cell.nil? and !@visited[cell.x][cell.y] }
          
          dir.each { |cell| @previous[cell] = current }
          @currents.push(*dir)
        end
      end
    end
  end

  def draw
    mainColor = Gosu::Color.rgba(144, 238, 144, 255)
    secondColor = Gosu::Color.rgba(144, 238, 144, 50)

    @width.times do |x|
      @height.times do |y|
        draw_cell(x, y, Gosu::Color.rgba(0,0,0,25)) if @visited[x][y]
      end
    end

    if @solved
      current = @to
      loop do
        prev = @previous[current]
        break if prev.nil?
        draw_cell(prev.x, prev.y, mainColor)        
        current = prev
      end
    else
      @currents.each do |current| 
        draw_cell(current.x, current.y, mainColor)
        loop do
          prev = @previous[current]
          break if prev.nil?
          draw_cell(prev.x, prev.y, secondColor)        
          current = prev
        end
      end
    end
    draw_cell(@from.x, @from.y, Gosu::Color::GREEN)
    draw_cell(@to.x, @to.y, Gosu::Color::RED)

    @maze.draw
  end

end