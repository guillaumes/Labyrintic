require 'gosu'
require 'pp'

require_relative 'generator/Depth.rb'
require_relative 'generator/Prim.rb'
require_relative 'generator/Kruskal.rb'

class MainWindow < Gosu::Window
  def initialize width, height, cellSize, generators, mazeFile, fullscreen
    @cellSize = cellSize
    @windowWidth = width - (width % @cellSize)
    @windowHeight = height - (height % @cellSize)
    
    super @windowWidth-1, @windowHeight
    self.fullscreen = fullscreen
    @backgroundColor = Gosu::Color::WHITE
    @wallsColor = Gosu::Color::BLACK

    @generators = generators
    @current_generator = 0
  
    initialize_generator @generators[@current_generator]
    @exported = false
    @mazeFile = mazeFile

    @pause = false
  end

  def initialize_generator with
    @generator = case with
      when 'depth'
        Depth.new (@windowWidth/@cellSize), (@windowHeight/@cellSize), @cellSize, @wallsColor
      when 'prim'
        Prim.new (@windowWidth/@cellSize), (@windowHeight/@cellSize), @cellSize, @wallsColor
      when 'kruskal'
        Kruskal.new (@windowWidth/@cellSize), (@windowHeight/@cellSize), @cellSize, @wallsColor
      end
    
    caption = "Generating with #{with}"
    self.caption = caption if self.caption != caption
  end

  def button_down id
    case id
      when 41 # escape
        # exit
      when 44 # space
        @pause = !@pause
      when 87 # +
        self.update_interval /= 2
      when 86 # -
        self.update_interval *= 2        
    end
    if self.update_interval < 1
      self.update_interval = 1.0
    elsif self.update_interval > 1024
      self.update_interval = 1024.0
    end

    if 86 <= id && id <= 87
      puts self.update_interval
    end
  end

  def update
    return if @pause

    if !@generator.generated
      @generator.step
      @since = Time.new
    end

    if @generator.generated && !@exported
      File.open(@mazeFile, 'w') { |file| file.write(@generator.maze.export) }
      @exported = true
    end

    if @generator.generated && (Time.new - @since) > 5
      @current_generator = (@current_generator + 1) % @generators.length
      initialize_generator @generators[@current_generator]
      @exported = false
    end
  end

  def needs_cursor?
    true
  end

  def draw
    Gosu.draw_rect(0,0,@windowWidth, @windowHeight, @backgroundColor)
    @generator.draw
  end
end


if ARGV.length == 0
  print "Window's width (default 800) : "
  width = gets.chomp.to_i
  width = 800 if width <= 0

  print "Window's height (default 600) : "
  height = gets.chomp.to_i
  height = 600 if height <= 0

  print "Cell's size (default 10) : "
  cellSize = gets.chomp.to_i
  cellSize = 10 if cellSize == 0 or cellSize < 2

  print "Fullscreen (default 0) : "
  fullscreen = gets.chomp.to_i
  fullscreen = fullscreen == 1

  generators = %w{kruskal depth prim}.shuffle
  mazeFile = 'maze/maze.sg'
else
  width = ARGV[0].to_i 
  height = ARGV[1].to_i
  cellSize = ARGV[2].to_i
  generators = ARGV[3].to_s.split ','
  mazeFile = ARGV[4].to_s
  fullscreen = ARGV[5].to_i > 0
end

MainWindow.new(width, height, cellSize, generators, mazeFile, fullscreen).show