class Cell
  attr_reader :x, :y
  attr_accessor :up, :left

  def initialize x, y, u = true, l = true
    @x = x
    @y = y
    @up = u
    @left = l
  end
end
