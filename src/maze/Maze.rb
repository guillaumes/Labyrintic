require_relative 'Cell.rb'
require_relative '../graph/Graph.rb'

class Maze
  attr_reader :width, :height, :cellSize, :cells, :wallsColor

  # width and height in number of cell
  def initialize w, h, cs, wc
    @width = w
    @height = h
    @cellSize = cs
    @wallsColor = wc

    clear()

    @generating = false
    @generated = false
    @solving = false
  end

  def clear
    @cells = Array.new(@width)

    @width.times do |x|
      @cells[x] = Array.new(@height)
      @height.times do |y|
        @cells[x][y] = Cell.new x,y
      end
    end
  end

  def update dt
  end

  def draw
    @cells.each_with_index do |row, x|
      row.each_with_index do |cell, y|
        Gosu.draw_line(x*@cellSize, y*@cellSize, @wallsColor, x*@cellSize+@cellSize, y*@cellSize, @wallsColor) if wallUp(cell)
        Gosu.draw_line(x*@cellSize, y*@cellSize, @wallsColor, x*@cellSize, y*@cellSize+@cellSize, @wallsColor) if wallLeft(cell)
      end
    end    
  end

  def cellUp cell
    if cell.x >= 0 and cell.x < @width and cell.y >= 1 and cell.y < @height
      return @cells[cell.x][cell.y-1]
    end
  end

  def cellDown cell
    if cell.x >= 0 and cell.x < @width and cell.y >= 0 and cell.y < @height-1
      return @cells[cell.x][cell.y+1]
    end
  end

  def cellLeft cell
    if cell.x >= 1 and cell.x < @width and cell.y >= 0 and cell.y < @height
      return @cells[cell.x-1][cell.y]
    end
  end

  def cellRight cell
    if cell.x >= 0 and cell.x < @width-1 and cell.y >= 0 and cell.y < @height
      return @cells[cell.x+1][cell.y]
    end
  end

  def wallUp cell, b=nil
    if b.nil?
      cell.up
    else
      cell.up = b
    end
  end

  def wallDown cell, b=nil
    return true if cellDown(cell).nil? 
    if b.nil?
      cellDown(cell).up
    else
      cellDown(cell).up = b
    end
  end

  def wallLeft cell, b=nil
    if b.nil?
      cell.left
    else
      cell.left = b
    end
  end

  def wallRight cell, b=nil
    return true if cellRight(cell).nil? 
    if b.nil?
      cellRight(cell).left
    else
      cellRight(cell).left = b
    end
  end

  def export
    data = ""
    @cells.each_with_index do |row, x|
      row.each_with_index do |cell, y|
        n = 0
        n += 1 if cell.up
        n += 2 if cell.left
        data += n.to_s
      end
      data += "\n"
    end
    data
  end

  def self.import data, cs, wc
    data = data.split.map{ |l| l.split '' }
    width = data.length
    height = data[0].length

    maze = Maze.new width, height, cs, wc

    data.each_with_index do |row, x|
      row.each_with_index do |n, y|
        i = n.to_i
        cell = maze.cells[x][y]
        
        up = (i == 1 or i == 3)
        left = (i == 2 or i == 3)
        maze.wallUp(cell, up)
        maze.wallLeft(cell, left)
      end
    end
    maze
  end

end
