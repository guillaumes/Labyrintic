Node = Struct.new("Node", :element, :weight)

class Heap #of step
  private
  attr_accessor :array, :weights, :length, :capacity, :indices

  def childs i
    [2*i+1, 2*i+2]
  end

  def parent i
    (i-1)/2
  end

  def compare a, b
    return false if @weights[b].nil?
    return true if @weights[a].nil?
    @weights[a] < @weights[b]
  end

  def swap i,j
    @indices[@array[i]], @indices[@array[j]] = @indices[@array[j]], @indices[@array[i]]
    @array[i], @array[j] = @array[j], @array[i]
  end

  def resize new_length
    if new_length > capacity
      @capacity *= 2
      nArray = Array.new @capacity
      nArray[0,length] = @array
      @array = nArray
    end
    @length = new_length
  end

  def find element
    return @array.find_index element
  end

  def adjust n
    c = childs(n)
    return if c.compact.length == 0
    if !@array[c[1]].nil? and compare(@array[c[1]], @array[n])
      swap(c[1],n)
      adjust c[1]
    end
    if !@array[c[0]].nil? and compare(@array[c[0]], @array[n])
      swap(c[0],n)
      adjust c[0]
    end
  end

  public
  def initialize
    @length = 0
    @capacity = 6
    @array = Array.new @capacity
    @weights = Hash.new
    @indices = Hash.new
  end

  def length
    @length
  end

  def pop
    e = @array[0]
    w = @weights[e]

    @array[0] = @array[@length-1]
    @array[@length-1] = nil

    adjust 0
    resize @length-1

    return Node.new(e,w)
  end

  def push element, value
    if !weights[element].nil?
      return false if weights[element] <= value
      i = @indices[element]
    else
      resize @length+1
      i = length - 1
      @array[i] = element
      @indices[element] = i
    end
    @weights[element] = value

    n = i
    while n > 0 and compare(@array[n], @array[parent(n)])
      swap n, parent(n)
      n = parent(n)
    end
  end

  def clear
    @capacity = 6
    @array = Array.new @capacity
    @length = 0
  end

  def to_a
    @array.compact
  end
end
