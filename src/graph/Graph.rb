require 'set'
require_relative 'Heap.rb'

class Graph
  attr_accessor :in, :out
  attr_reader :nodes, :links

  # initializers

  def initialize
    @nodes = Set.new
    @links = Hash.new
  end

  def self.from_maze maze, win, wout
    t = Time.new
    graph = Graph.new

    graph.in = win
    graph.out = wout

    width = maze.width
    height = maze.height

    0.upto height-1 do |j|
      last = nil
      0.upto width-1 do |i|
        cell = maze.cells[i][j]

        last = nil if maze.wallLeft(cell)

        if !(maze.wallUp(cell) and maze.wallDown(cell) and !maze.wallLeft(cell) and !maze.wallRight(cell)) and !(!maze.wallUp(cell) and !maze.wallDown(cell) and maze.wallLeft(cell) and maze.wallRight(cell))
          graph.nodes << cell 
          graph.link cell, last if !last.nil?

          k = j-1
          while k >= 0 && !maze.wallDown(maze.cells[i][k])
            above_cell = maze.cells[i][k]
            if graph.nodes.include? above_cell then
              graph.link cell, above_cell
              break
            end
            k -= 1
          end

          last = cell
        end
      end
    end

    before = graph.nodes.count

    loop do
      i = 0
      graph.nodes.each do |cell|
        if graph.links[cell].length == 2 and cell != graph.in and cell != graph.out
          ab = graph.links[cell].to_a
          a = ab[0]
          b = ab[1]
          graph.unlink a,cell
          graph.unlink b,cell
          graph.remove_node cell
          graph.link a,b
          i += 1
        end
      end
      break if i == 0
    end

    puts "#{graph.nodes.count}/#{before} = #{graph.nodes.count.to_f/before}"    
    puts "Graph generated in #{Time.new-t}"
    graph
  end

  # Methods

  def add_node value
    @nodes << value
  end

  def remove_node value
    @nodes.delete value
  end

  def link a,b
    @links[a] = Set.new if @links[a].nil?
    @links[a] << b

    @links[b] = Set.new if @links[b].nil?
    @links[b] << a
  end

  def unlink a,b
    @links[a].delete b if !@links[a].nil?
    @links[b].delete a if !@links[b].nil?
  end

  # Dijkstra from @in to @out
  def dijkstra
    heap = Heap.new
    heap.push @in, 0
    done = Set.new # Set of Point
    path = Hash.new # Point => Point (node to previous)

    while heap.length > 0
      current = heap.pop

      break if current.element == @out
      if done.include? current.element
        puts "This should not happen"
        next
      end

      @links[current.element].each do |next_node|
        if !done.include?(next_node) then
          heap.push next_node, current.weight+distance(current.element,next_node)
          path[next_node] = current.element
        end
      end
      done << current.element
    end

    return done.to_a if path[@out].nil?

    qpath = Array.new
    qpath << @out
    while !(path[qpath.last].nil? or qpath.last == @in)
      qpath << path.delete(qpath.last)
    end

    qpath.reverse
  end
end

def distance p,q
  (p.x-q.x).abs + (p.y-q.y).abs
end