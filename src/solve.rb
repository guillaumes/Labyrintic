require 'gosu'
require 'pp'

require_relative 'maze/Maze.rb'
require_relative 'solver/Flood.rb'
require_relative 'solver/Depth.rb'
require_relative 'solver/AStar.rb'

class MainWindow < Gosu::Window
  def initialize cellSize, solvers, mazeFile, fullscreen
    @cellSize = cellSize
    @backgroundColor = Gosu::Color::WHITE
    @wallsColor = Gosu::Color::BLACK
    super 1,1
    self.fullscreen = fullscreen
    
    @solvers = solvers
    @current_solver = 0
    @mazeFile = mazeFile
    
    load_maze()
    initialize_solver @solvers[@current_solver]
  end
  
  def load_maze
    data = File.read(@mazeFile)
    @maze = Maze.import data, @cellSize, @wallsColor

    width = @cellSize*@maze.width
    height = @cellSize*@maze.height
    self.width = width if self.width != width
    self.height = height if self.height != height

    @pause = false
  end

  def initialize_solver with
    loop do
      @from = @maze.cells[Random.rand @maze.width][Random.rand @maze.height]
      @to = @maze.cells[Random.rand @maze.width][Random.rand @maze.height]
      break if @from.x != @to.x or @from.y != @to.y
    end

    @solver = case with
      when 'flood'
        Flood.new @maze, @from, @to
      when 'depth'
        Depth.new @maze, @from, @to
      when 'astar'
        AStar.new @maze, @from, @to
      end
    
    caption = "Solving with #{with}"    
    self.caption = caption if self.caption != caption
  end

  def button_down id
    case id
      when 41 # escape
        exit
      when 44 # space
        @pause = !@pause
      when 87 # +
        self.update_interval /= 2
      when 86 # -
        self.update_interval *= 2        
    end
    if self.update_interval < 1
      self.update_interval = 1.0
    elsif self.update_interval > 1024
      self.update_interval = 1024.0
    end

    if 86 <= id && id <= 87
      puts self.update_interval
    end
  end

  def update
    return if @pause    

    if !@solver.solved
      @solver.step
      @since = Time.new
    end

    if @solver.solved && (Time.new - @since) > 5
      load_maze()
      @current_solver = (@current_solver + 1) % @solvers.length
      initialize_solver @solvers[@current_solver]
    end
  end

  def needs_cursor?
    true
  end

  def draw
    Gosu.draw_rect(0, 0, self.width, self.height, @backgroundColor)
    @solver.draw
  end
end


if ARGV.length == 0
  print "Cell's size (default 10) : "
  cellSize = gets.chomp.to_i
  cellSize = 10 if cellSize == 0 or cellSize < 2

  print "Fullscreen (default 0) : "
  fullscreen = gets.chomp.to_i
  fullscreen = 0 if fullscreen.nil?

  solvers = %w{flood depth astar}.shuffle
  mazeFile = 'maze/maze.sg'
else
  cellSize = ARGV[0].to_i
  solvers = ARGV[1].to_s.split ','
  mazeFile = ARGV[2].to_s
  fullscreen = ARGV[3].to_i > 0
end

MainWindow.new(cellSize, solvers, mazeFile, fullscreen).show