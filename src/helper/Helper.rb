require 'gosu'

class Helper
   
  def initialize
    raise 'You are trying to instantiate an abstract class!'
  end

  def step
    raise 'You must implement step'
  end

  def draw
    raise 'You must implement draw'
  end

  def draw_cell x, y, c
    a = x*@cellSize
    Gosu.draw_rect(x*@cellSize, y*@cellSize, @cellSize, @cellSize, c)
  end
end